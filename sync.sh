#!/bin/sh

for Dir in *; do
    if [ -d "$Dir/.git" ]; then
        echo "Updating Directory: $Dir"
		pushd $Dir > /dev/null
		git pull origin master
		git push origin master
		popd > /dev/null
    fi
done

[ $SHLVL -eq 1 ] && read -p "Press Enter to Exit..."
