/* Stephan Moncavage
 * CST235
 * 18 August 2018
 * This is my own work. 
 */
package contentpackage;

import java.io.IOException;
import java.beans.*;
/**
 *
 * @author monca
 */
public class InputDriver {
    public static String file = ("C:\\Users\\monca\\Documents\\dev\\Samples\\4dostips.txt");
    public static void main(String[] args) throws IOException, IntrospectionException {
        
        SessionInputClass.setFilename(file);
   
        NumberofChar.readFileReturnIntegers(SessionInputClass.getFilename()); 
        NumberofWords.readFileReturnIntegers(SessionInputClass.getFilename());
        NumberofInt.readFileReturnIntegers(SessionInputClass.getFilename()); 
         
       
    }
    
}
