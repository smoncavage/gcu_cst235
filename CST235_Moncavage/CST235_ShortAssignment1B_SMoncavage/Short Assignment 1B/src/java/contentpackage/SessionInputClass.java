/* Stephan Moncavage
 * CST235
 * 18 August 2018
 * This is my own work. 
 */
package contentpackage;

import java.util.*;
import java.io.*;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

@Stateless
@LocalBean
public class SessionInputClass implements Serializable{
    public static final int x = 200000;
    public static final int y = 450000;
    public static int numofchar = 0;
    public static int numofwords = 0;
    public static int numofint = 0;
    public static char[] file = new char[y];
    public static String[] stringarray = new String[y];
    public static ArrayList<Integer> numbers = new ArrayList<Integer>();   
    public static String filename;
    
        
    public static void main(String[] args) throws IOException {
        NumberofChar.readFileReturnIntegers(filename); 
        NumberofWords.readFileReturnIntegers(filename);
        NumberofInt.readFileReturnIntegers(filename);
    }
     public static int getNumofchar() {
		return numofchar;
	}
	public static void setNumofchar(int numofchar) {
		SessionInputClass.numofchar = numofchar;
	}
	public static int getNumofwords() {
		return numofwords;
	}
	public static void setNumofwords(int numofwords) {
		SessionInputClass.numofwords = numofwords;
	}
	public static int getNumofint() {
		return numofint;
	}
	public static void setNumofint(int numofint) {
		SessionInputClass.numofint = numofint;
	}
	public static char[] getFile() {
		return file;
	}
	public static void setFile(char[] file) {
		SessionInputClass.file = file;
	}
	public static String[] getStringarray() {
		return stringarray;
	}
	public static void setStringarray(String[] stringarray) {
		SessionInputClass.stringarray = stringarray;
	}
	public static ArrayList<Integer> getNumbers() {
		return numbers;
	}
	public static void setNumbers(ArrayList<Integer> numbers) {
		SessionInputClass.numbers = numbers;
	}
	public static String getFilename() {
		return filename;
	}
	public static void setFilename(String n) {
		SessionInputClass.filename = n;
	}
}
class NumberofInt2 {
    public static Integer readFileReturnIntegers(String filename) {
        
        try (Scanner br = new Scanner(new FileInputStream(filename))) {
                String line;
                while (br.hasNextLine()){
                    try{
                        int j = 0;
                    Integer i = br.nextInt();
                    j += i; 
                    SessionInputClass.numofint +=i;
                    try {
			Thread.sleep(10);
                    } catch (InterruptedException e) {
			e.printStackTrace();
                    }
                    } catch (InputMismatchException e){  
                        br.next();
                    }   
                    }               
                }catch (IOException e) {
        }         
        System.out.println("The total sum of all integers within this file is: " + SessionInputClass.getNumofint());                
        return SessionInputClass.getNumofint();
    }    
}

class NumberofChar2 {
    public static Integer readFileReturnIntegers(String filename) throws FileNotFoundException, IOException{
        BufferedReader br = new BufferedReader(new FileReader(filename));
        int count = 0;        
        String line = br.readLine();
        while (line != null){
            char[] parts = line.toCharArray();
                for( char w : parts){
                    count++;        
                }
            line = br.readLine();
        }
        SessionInputClass.setNumofchar(count);
        try {
            Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        System.out.println("The total sum of all characters within this file is: " + SessionInputClass.getNumofchar());                
        return SessionInputClass.getNumofchar();
    }
}

class NumberofWords2 {
    public static Integer readFileReturnIntegers(String filename) throws IOException{       
        BufferedReader br = new BufferedReader(new FileReader(filename));
        int count = 0;        
        String line = br.readLine();
        while (line != null){
            String []parts = line.split(" ");
                for( String w : parts){
                    count++;        
                }
            line = br.readLine();
        }         
       SessionInputClass.setNumofwords(count);          
        System.out.println("The total sum of all words within this file is: " + SessionInputClass.getNumofwords());                        
        return SessionInputClass.getNumofwords();
    }
    public static int countWords(String line){
        String words[]=line.split(" ");
        int count=words.length;
        return count;
    }
}