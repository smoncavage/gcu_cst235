/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestEJBpkg;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class Print {

    @Inject
    RegJavaBean sessbean;
    
    private String name;
    private static String welcome;

    public void setWelcome() {
        Print.welcome = RegJavaBean.hello("Hello "+ name + " welcom to the faces inject servlet.");
    }

    public String getWelcome() {
        return welcome;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
