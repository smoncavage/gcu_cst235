/* Stephan Moncavage
 * CST235
 * 14 August 2018
 * This is my own work. 
 */
package TestEJBpkg;

import java.io.Serializable;
import javax.enterprise.context.Dependent;

@Dependent
public class RegJavaBean implements Serializable{
        
        static String cola;
	static String name;
	double drinkPrice;
        
        public static String hello(String name) {
        return "Hello, " + name + " would you like a drink?";
    }
        
        public void setCola(){
            this.cola = cola;
        }
        
        public String getCols (){
            return cola;
        }
        public void setName(String name){
            this.name = name;
        }
        
        public String getName (){
            return name;
        }
        
}
