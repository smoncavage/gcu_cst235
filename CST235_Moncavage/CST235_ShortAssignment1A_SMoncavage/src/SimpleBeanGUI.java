/* Stephan Moncavage
 * CST235
 * 16 August 2018
 * This is my own work. 
 */
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class SimpleBeanGUI extends Application {
	
	//Set up variables to be used throughout the code
	public static final Button Calc = new Button ("Calculate");
	public static final Label label = new Label("Video Duration in seconds");
	public static final Label label1 = new Label("Storage in MB");
	public static final Label label2 = new Label("Storage in GB");
	public static final Label label3 = new Label();
	public static final Label label4 = new Label();
	public static final CheckBox checkbox1 = new CheckBox("720p");
	public static final CheckBox checkbox2 = new CheckBox("1080p");
	public static final Spinner<Integer> spinner1 = new Spinner<>(1, 1000000, 1);
	public static final int low = 720;
	public static final int high = 1080;
	
	//public static void main(String[] args) {
		
	//}
	
	@Override
	public void start(Stage primaryStage) { //Set the stage for the GUI with components
		GridPane mainScreen = new GridPane();
		mainScreen.setAlignment(Pos.CENTER);
		mainScreen.setPadding(new Insets(11, 12, 13, 14));
		mainScreen.setHgap(5);
		mainScreen.setVgap(5);
		mainScreen.add(checkbox1, 1, 1);
		mainScreen.add(checkbox2, 1, 2);
		mainScreen.add(spinner1, 1, 3);
		mainScreen.add(label, 2, 3);
		mainScreen.add(Calc, 1, 4);
		mainScreen.add(label3, 1, 5);
		mainScreen.add(label4, 1, 6);
		mainScreen.add(label1, 2, 5);
		mainScreen.add(label2, 2, 6);
		
		Scene scene = new Scene(mainScreen);
		scene.fillProperty().set(Color.FUCHSIA);
		primaryStage.setTitle("Vending Machine");
		primaryStage.setScene(scene);
		primaryStage.show();	//Show the Stage
		
		spinner1.setEditable(true);
		
		Calc.setOnAction(new EventHandler<ActionEvent>(){ //Button initiates calculation of values
			@Override
			public void handle (ActionEvent event) {				
				Calc.isPressed();				
				if(checkbox1.isSelected()){
		            SimpleJavaBean.setPixels(low);
		            try {
			            Thread.sleep(10);
			        } catch (InterruptedException k) {					
			            k.printStackTrace();
			        }
		            
		        }
		        if(checkbox2.isSelected()){
		            SimpleJavaBean.setPixels(high);
		            try {
			            Thread.sleep(10);
			        } catch (InterruptedException k) {					
			            k.printStackTrace();
			        }
		            
		        }  		        
		        SimpleJavaBean.setDuration(spinner1.getValue());
		        SimpleJavaBean.setStorageCalcMB();
		        try {
		            Thread.sleep(10);
		        } catch (InterruptedException k) {					
		            k.printStackTrace();
		        }
		        SimpleJavaBean.setStorageCalcGB();		        
		        
		        try {
		            Thread.sleep(1000);
		        } catch (InterruptedException k) {					
		            k.printStackTrace();
		        }
		        
		        SimpleJavaBean.setStoreMBText();
		        SimpleJavaBean.setStoreGBText();
		        
		        label3.setText(SimpleJavaBean.getStoreMBText());
		        label4.setText(SimpleJavaBean.getStoreGBText());
		        
				primaryStage.setScene(scene); //Displays the results of the calculation in the same window
				primaryStage.show();				
			}
		});
	}

}
