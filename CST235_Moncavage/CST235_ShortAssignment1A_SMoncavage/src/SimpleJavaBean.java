/* Stephan Moncavage
 * CST235
 * 14 August 2018
 * This is my own work. 
 */
import java.io.Serializable;
import javafx.scene.text.Text;

public class SimpleJavaBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6567468231140357634L;
	public static int pixels; //Two acceptable values are 720p (1280X720) or 1080p (1920X1080)
    public static int duration; //Duration is calculated in sec.
    public static double storagereqMB;
    public static double storagereqGB;
    public static String StoreMB;
    public static String StoreGB;
    public static Text strgMB;
    public static Text strgGB;
    
    public SimpleJavaBean() {
    	
    }
    
    public static void setPixels(int i) {
       pixels = i;
    }    
    public static int getPixels(){
        return pixels;
    }    
    public static void setDuration (Integer integer){
    	
        duration = integer;
    }
    public static int getDuration(){    	
        return duration;
    }   
    public static void setStorageCalcMB(){  	
    	double storageMB = (getPixels() * getDuration());
        storagereqMB = (int) ((double) Math.round(storageMB * 100) / 100);       
    }    
    public static double getStorageCalcMB() {
    	return  storagereqMB;
    }
    public static void setStorageCalcGB() {
    	
    	double storageGB = (getStorageCalcMB() / 1024.0);
        storagereqGB = ((storageGB * 10) / 10);     
    }   
    public static double getStorageCalcGB() {
    	return storagereqGB;
    }   
    public static void setStoreMBText() {
    	
    	StoreMB = String.valueOf(getStorageCalcMB());
    }   
    public static String getStoreMBText() {
    	strgMB = new Text(StoreMB);
        
        return StoreMB;
    }    
    public static void setStoreGBText() {
    	
    	StoreGB = String.valueOf(getStorageCalcGB());
    }    
    public static String getStoreGBText() {
    	strgGB = new Text(StoreGB);
        
        return StoreGB;
    }
}